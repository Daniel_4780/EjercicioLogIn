package curso.umg.gt.ejerciciologin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etNombre, etApellido, etContrasena, etConfirmaConstrasena;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNombre = (EditText) findViewById(R.id.etNombre);
        etApellido = (EditText) findViewById(R.id.etApellido);
        etContrasena = (EditText) findViewById(R.id.etContrasea);
        etConfirmaConstrasena = (EditText) findViewById(R.id.etconfirmeContrasen);
    }

    public void ValidarContrasena(View view){
        String contrasen = etContrasena.getText().toString();
        String confirmarContrasena = etConfirmaConstrasena.getText().toString();

        if(contrasen.equals(confirmarContrasena))
        {
            Intent informacion = new Intent(this, InformacionActivity.class);
            startActivity(informacion);
        }
        else{
            Toast notificacionCredencialeas = Toast.makeText(this, "Credenciales invalidas", Toast.LENGTH_SHORT);
            notificacionCredencialeas.show();
        }
    }

    public void Limpira(View view){
        etNombre.setText("");
        etApellido.setText("");
        etContrasena.setText("");
        etConfirmaConstrasena.setText("");
    }
}
