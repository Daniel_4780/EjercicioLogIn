package curso.umg.gt.ejerciciologin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

public class InformacionActivity extends AppCompatActivity {

    RadioButton rbAndroidSi, rbAndroidNo, rbJavaSi, rbJavaNo, rbSpringSi, rbSpringNo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion);

        rbAndroidSi = (RadioButton) findViewById(R.id.rbAndroiSi);
        rbAndroidNo = (RadioButton) findViewById(R.id.rbAndroiNo);
        rbJavaSi = (RadioButton) findViewById(R.id.rbJavaSi);
        rbJavaNo = (RadioButton) findViewById(R.id.rbJavaNo);
        rbSpringSi = (RadioButton) findViewById(R.id.rbSpringSi);
        rbSpringNo = (RadioButton) findViewById(R.id.rbSpringNo);
    }

    public void preferencia(View view){

        String mensaje = "Lista de preferencia \n";

        if(rbAndroidSi.isChecked()){
            mensaje = mensaje + "Java \n";
        }
        if(rbJavaSi.isChecked()){
            mensaje = mensaje + "Android \n";
        }

        if(rbSpringSi.isChecked()){
            mensaje = mensaje + "Spring \n";
        }

        Toast notificacion = Toast.makeText(this, mensaje,Toast.LENGTH_LONG);
        notificacion.show();
    }
}
